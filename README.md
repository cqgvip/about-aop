### 关于AOP 和 字节码


#### 字节码相关

- ASM
- javassist
- BCEL

#### 动态代理

- javaAPI  Proxy
- cglib

### AOP

- aspectJ
- fastAOP
- spring aop

### Agent

两种方式
1. premain    运行前指定
2. agentmain  运行后动态附着


### 实测
t1:java原生调用  t2:java反射调用  t3:javassist调用
循环执行10次 每次内循环1亿次 耗时对比
```
1->t1:37 t2:218 t3:30
2->t1:31 t2:225 t3:31
3->t1:29 t2:214 t3:27
4->t1:27 t2:213 t3:26
5->t1:27 t2:211 t3:27
6->t1:27 t2:213 t3:26
7->t1:27 t2:211 t3:27
8->t1:27 t2:212 t3:26
9->t1:27 t2:213 t3:27
10->t1:26 t2:212 t3:26
```

#### 结论
> 原生调用 和 javassist生成代码调用 耗时一样
> 反射调用耗时是原生调用的7倍左右
