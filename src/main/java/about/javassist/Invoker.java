package about.javassist;

public interface Invoker {
    Object invoke(String name);
}
