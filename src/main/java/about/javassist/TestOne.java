package about.javassist;

import javassist.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class TestOne {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, CannotCompileException, NotFoundException, InstantiationException {
        final RealService rs = new RealService();
        final Method sayOne = RealService.class.getDeclaredMethod("sayOne");
        long num=100_000_000L;
        final Invoker invoker = getInvoker();

        for (int j = 0; j < 100; j++) {
            long t1d,t2d,t3d=0;
            {
                final long ts = System.currentTimeMillis();
                for (int i = 0; i < num; i++) {
                    rs.sayOne();
                }
                long t = System.currentTimeMillis() - ts;
//                System.out.println("t2:"+t2);
                t1d=t;
            }
            {
                final long ts = System.currentTimeMillis();
                for (int i = 0; i < num; i++) {
                    sayOne.invoke(rs);
                }
                long t = System.currentTimeMillis() - ts;
//                System.out.println("t1:" + t1);
                t2d=t;
            }
            {

                final long ts = System.currentTimeMillis();
                for (int i = 0; i < num; i++) {
                    invoker.invoke("sayOne");
                }
                long t = System.currentTimeMillis() - ts;
//                System.out.println("t2:"+t2);
                t3d=t;
            }

            System.out.println(String.format("%s->t1:%s t2:%s t3:%s",j,t1d,t2d,t3d));
        }


    }

    public static Invoker getInvoker() throws NotFoundException, CannotCompileException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final ClassPool cp = ClassPool.getDefault();
        final CtClass cls = cp.makeClass("my.Some");
        final CtClass proxyClazz = cp.getCtClass("about.javassist.RealService");
        cls.addInterface(cp.getCtClass("about.javassist.Invoker"));
        final CtField ctField = new CtField(proxyClazz, "proxy", cls);
        cls.addField(ctField);
        final CtConstructor ctConstructor = new CtConstructor(new CtClass[]{proxyClazz}, cls);
        ctConstructor.setBody("{$0.proxy=$1;}");
        cls.addConstructor(ctConstructor);
        final StringBuffer sb = new StringBuffer();
        sb.append("{\n");
        final Method[] ms = RealService.class.getDeclaredMethods();
        sb.append("if(true) return $0.proxy.sayOne();");
        for (int i = 0; i < ms.length; i++) {
            final Method m = ms[i];
            final String name = m.getName();
            if(i!=0){
                sb.append("\n else ");
            }
//            String stement = String.format("if(%s.equels($1)){return $0.proxy.%s();}", name, name);
//            String stement = String.format("if(\"%s\".equals($1)){return \"hello %s\";}", name, name);
            String stement = String.format("if(\"%s\".equals($1)){return \"hello %s\";}", name, name);
            sb.append(stement);
        }
        sb.append("\nthrow new RuntimeException(\"method not found\");\n}");
        final String st = sb.toString();
        System.out.println(st);
        final CtClass objClass = cp.getCtClass("java.lang.Object");
        final CtClass strClass = cp.getCtClass("java.lang.String");
        final CtMethod method = new CtMethod(objClass,
                "invoke",
                new CtClass[]{strClass}, cls);
        method.setModifiers(Modifier.PUBLIC);
        method.setBody(st);
        cls.addMethod(method);
        final Constructor<?> declaredConstructor = cls.toClass().getDeclaredConstructor(RealService.class);
        final Object o = declaredConstructor.newInstance(new RealService());
        final Invoker invoker = (Invoker) o;
        final Object sayOne = invoker.invoke("sayOne");
        System.out.println(sayOne);
        return invoker;
    }
}
